package org.example.signed_messages;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

public class Keyring {
    private static final String TAG = "Keyring";

    public static String serializePublicKey(PublicKey publicKey) {
        return Base64.encodeToString(publicKey.getEncoded(), Base64.DEFAULT);
    }

    public KeyPair getExistingKeyPairOrGenerate(MainActivity mainActivity) {
        SharedPreferences  mPrefs = mainActivity.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();

        KeyPair keyPair = loadKeyPair(mPrefs);
        if (keyPair != null) {
            Log.w(TAG, "Keypair found");
            return keyPair;
        } else {
            KeyPair newKeyPair = generateKeyPair();
            saveKeyPair(prefsEditor, newKeyPair);
            Toast.makeText(mainActivity, "New key pair generated", Toast.LENGTH_SHORT).show();
            return newKeyPair;
        }

    }

    private void saveKeyPair(SharedPreferences.Editor prefsEditor, KeyPair keyPair) {
        String publicKeySerialized = serializePublicKey(keyPair.getPublic());
        String privateKeySerialized = Base64.encodeToString(keyPair.getPrivate().getEncoded(), Base64.DEFAULT);
        prefsEditor.putString("publicKey", publicKeySerialized);
        prefsEditor.putString("privateKey", privateKeySerialized);
        prefsEditor.commit();
    }

    private KeyPair loadKeyPair(SharedPreferences mPrefs) {
        String publicKeySerialized = mPrefs.getString("publicKey", "");
        String privateKeySerialized = mPrefs.getString("privateKey", "");
        if (publicKeySerialized.equals("") || privateKeySerialized.equals("")) return null;

        byte[] publicKeyBytes = Base64.decode(publicKeySerialized, Base64.DEFAULT);
        byte[] privateKeyBytes = Base64.decode(privateKeySerialized, Base64.DEFAULT);
        X509EncodedKeySpec pubKeySpec = new X509EncodedKeySpec(publicKeyBytes);
        PKCS8EncodedKeySpec priKeySpec = new PKCS8EncodedKeySpec(privateKeyBytes);

        return buildKeyPairFromKeySpecs(pubKeySpec, priKeySpec);
    }

    @NonNull
    private KeyPair buildKeyPairFromKeySpecs(X509EncodedKeySpec pubKeySpec, PKCS8EncodedKeySpec priKeySpec) {
        PublicKey publicKey = null;
        PrivateKey privateKey = null;
        try {
            KeyFactory keyFactory = getKeyFactory();
            publicKey = keyFactory.generatePublic(pubKeySpec);
            privateKey = keyFactory.generatePrivate(priKeySpec);
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
        return new KeyPair(publicKey, privateKey);
    }

    private KeyFactory getKeyFactory() {
        KeyFactory keyFactory = null;
        try {
            keyFactory = KeyFactory.getInstance("RSA");
        } catch (NoSuchAlgorithmException e) {
            // cannot happen as we support only RSA and it **must** be implemented by
            // KeyPairGenerator
            e.printStackTrace();
        }
        return keyFactory;
    }


    private KeyPair generateKeyPair() {
        KeyPair keyPair = null;
        try {
            KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
            kpg.initialize(2048);
            keyPair = kpg.generateKeyPair();
        } catch (NoSuchAlgorithmException e) {
            // cannot happen as we support only RSA and it **must** be implemented by
            // KeyPairGenerator
            e.printStackTrace();
        }
        return keyPair;
    }
}
