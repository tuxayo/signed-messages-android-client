package org.example.signed_messages;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;


public class SendMessageAsyncTask extends AsyncTask <String, Void, Boolean> {
    private static final String TAG = "SendMessageAsyncTask";
    public static final int PORT_NUMBER = 7070;
    private final MainActivity mainActivity;

    public SendMessageAsyncTask(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Override
    protected Boolean doInBackground(String... params) {
        String serverHostname = params[0];
        String message = params[1];
        String response = "";

        try {
            Socket socket = new Socket(serverHostname, PORT_NUMBER);
            PrintWriter writer =
                    new PrintWriter(socket.getOutputStream(), true);
            BufferedReader reader =
                    new BufferedReader(
                            new InputStreamReader(socket.getInputStream()));
            writer.println(message);
            Log.w(TAG, "SENT:" + message);

            // send and wait ACK
            response = reader.readLine();
            Log.w(TAG, "RESPONSE:" + response);

        } catch (UnknownHostException e) {
            System.err.println("Don't know about host " + serverHostname);
            System.exit(1);
        } catch (IOException e) {
            System.err.println("Couldn't get I/O for the connection to " +
                    serverHostname);
            System.exit(1);
        }
        return response.equals("ACK");
    }

    protected void onPostExecute(Boolean success) {
        if (success) {
            Toast.makeText(mainActivity, "Message was accepted by the server", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(mainActivity, "Message was rejected by the server", Toast.LENGTH_SHORT).show();
        }
    }

}
