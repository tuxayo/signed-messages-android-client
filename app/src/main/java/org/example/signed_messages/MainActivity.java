package org.example.signed_messages;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.SignatureException;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void connectAndSendMessage(View view) throws SignatureException, InvalidKeyException {
        String serverHostname = ((EditText)findViewById(R.id.serverHostname)).getText().toString();
        KeyPair keyPair = new Keyring().getExistingKeyPairOrGenerate(this);

        String messageContent = ((EditText)findViewById(R.id.message)).getText().toString();
        String publicKey = Keyring.serializePublicKey(keyPair.getPublic());
        String signature = new MessageSigner().sign(messageContent, keyPair);

//      I know that sending the public key with the message defeats the purpose
//      of signing but key distribution is beyond this project's scope.
        String message = buildMessageJson(messageContent, signature, publicKey);

        String[] sendingParams = {serverHostname, message};
        new SendMessageAsyncTask(this).execute(sendingParams);
    }

    private String buildMessageJson(String messageContent, String signature, String publicKey) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("message", messageContent);
            jsonObject.put("signature", signature);
            jsonObject.put("public_key", publicKey);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
