package org.example.signed_messages;

import android.util.Base64;

import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.Signature;
import java.security.SignatureException;

public class MessageSigner {
    private static String TAG = "MessageSigner";

    public String sign(String messageContent, KeyPair keyPair) throws InvalidKeyException, SignatureException {
        Signature signer = getRsaSha256Signature();
        signer.initSign(keyPair.getPrivate());
        signer.update(messageContent.getBytes());
        byte[] signatureBytes = signer.sign();
        String signatureString = Base64.encodeToString(signatureBytes, Base64.DEFAULT);
        return signatureString;
    }

    private Signature getRsaSha256Signature() {
        Signature sg = null;
        try {
            sg = Signature.getInstance("SHA256withRSA");

        } catch (NoSuchAlgorithmException e) {
            // cannot happen as we support only SHA256withRSA and it **must**
            // be implemented by Signature
            e.printStackTrace();
        }
        return sg;
    }
}
